package org.igorkonyukhov.weatherapi

import kotlinx.coroutines.*
import kotlinx.coroutines.test.runTest
import org.junit.Test

class WeatherApiProxyTest {
    private val apiProxy = WeatherApiProxy(createOpenWeatherApi(
        buildRetrofit(buildOkHttpClient())
    ))
    private val lat = 55.75f
    private val lon = 49.18f

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testWeatherRequest() = runTest {
        launch(Dispatchers.IO) {
            val response = apiProxy.getWeather(
                latitude = lat, longitude = lon
            )
            assert(response != null)
            println(response)
        }.join()
    }
}