package org.igorkonyukhov.weatherapi

class WeatherApiProxy(
    private val openWeatherApi: OpenWeatherApi
) {
    suspend fun getWeather(latitude: Float, longitude: Float): OpenWeatherResponse? =
        openWeatherApi.getWeather(latitude, longitude).body()
}
