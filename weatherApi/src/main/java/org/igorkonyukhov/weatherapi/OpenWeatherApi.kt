package org.igorkonyukhov.weatherapi

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

private const val apiKey = "69587a117c64bf9e456ed2692c7deb02"

interface OpenWeatherApi {
    @GET("weather")
    suspend fun getWeather(
        @Query("lat") latitude: Float,
        @Query("lon") longitude: Float,
        @Query("appid") appid: String = apiKey,
        @Query("units") units: String = "metric",
    ): Response<OpenWeatherResponse>
}