package org.igorkonyukhov.weatherapi

import java.net.URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val apiVersion = "2.5"
private const val baseUrlString = "https://api.openweathermap.org/data/$apiVersion/"
private val baseURL = URL(baseUrlString)

fun buildOkHttpClient(): OkHttpClient =
    OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        .build()

fun buildRetrofit(httpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(baseURL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

fun createOpenWeatherApi(retrofit: Retrofit): OpenWeatherApi = retrofit.create(OpenWeatherApi::class.java)