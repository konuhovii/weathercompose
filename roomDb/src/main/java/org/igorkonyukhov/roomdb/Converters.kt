package org.igorkonyukhov.roomdb

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun coordToString(obj: Coord?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToCoord(data: String?): Coord? = gson.fromJson(data, Coord::class.java)

    @TypeConverter
    fun weatherListToString(obj: List<Weather>?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToWeatherList(data: String?): List<Weather>? =
        gson.fromJson(data, object : TypeToken<List<Weather>>() {}.type)

    @TypeConverter
    fun mainToString(obj: Main?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToMain(data: String?): Main? = gson.fromJson(data, Main::class.java)

    @TypeConverter
    fun windToString(obj: Wind?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToWind(data: String?): Wind? = gson.fromJson(data, Wind::class.java)

    @TypeConverter
    fun snowToString(obj: Snow?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToSnow(data: String?): Snow? = gson.fromJson(data, Snow::class.java)

    @TypeConverter
    fun cloudsToString(obj: Clouds?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToClouds(data: String?): Clouds? = gson.fromJson(data, Clouds::class.java)

    @TypeConverter
    fun sysToString(obj: Sys?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToSys(data: String?): Sys? = gson.fromJson(data, Sys::class.java)
}