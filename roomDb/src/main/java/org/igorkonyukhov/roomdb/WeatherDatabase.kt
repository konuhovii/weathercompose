package org.igorkonyukhov.roomdb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [WeatherDBO::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao

    companion object {
        @Volatile
        private var instance: WeatherDatabase? = null

        @Synchronized
        fun getInstance(applicationContext: Context) = instance ?: Room.databaseBuilder(
            applicationContext,
            WeatherDatabase::class.java, "weather"
        ).build().also { instance = it }
    }
}