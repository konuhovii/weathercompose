package org.igorkonyukhov.roomdb

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
abstract class WeatherDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(weather: WeatherDBO)

    @Query("SELECT * FROM weather ORDER BY id ASC LIMIT 1")
    abstract fun getWeather(): Flow<WeatherDBO?>
}