package org.igorkonyukhov.weathercompose.dataSource

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.igorkonyukhov.roomdb.Clouds
import org.igorkonyukhov.roomdb.Coord
import org.igorkonyukhov.roomdb.Main
import org.igorkonyukhov.roomdb.Snow
import org.igorkonyukhov.roomdb.Sys
import org.igorkonyukhov.roomdb.Weather
import org.igorkonyukhov.roomdb.WeatherDBO
import org.igorkonyukhov.roomdb.WeatherDatabase
import org.igorkonyukhov.roomdb.Wind
import org.igorkonyukhov.weathercompose.domain.MainEntity
import org.igorkonyukhov.weathercompose.domain.OpenWeatherEntity
import org.igorkonyukhov.weathercompose.domain.WeatherEntity

class WeatherRoomLocalDataSource(private val weatherDatabase: WeatherDatabase) : IWeatherLocalDataSource {
    override suspend fun save(response: WeatherResponse) {
        weatherDatabase.weatherDao().insert(response.toDBO())
    }

    override fun getWeather(): Flow<OpenWeatherEntity?> =
        weatherDatabase.weatherDao().getWeather().map { it?.toEntity() }

    private fun WeatherDBO.toEntity(): OpenWeatherEntity =
        OpenWeatherEntity(weather = weather.map { WeatherEntity(it.main, it.description, it.icon) },
            main = main.let {
                MainEntity(it.temp, it.feelsLike, it.tempMin, it.tempMax, it.pressure, it.humidity)
            })

    private fun String.toIconUrlString() = "https://openweathermap.org/img/wn/$this@2x.png"

    private fun WeatherResponse.toDBO(): WeatherDBO = WeatherDBO(
        coord = coord.let {
            Coord(it.lon, it.lat)
        },
        weather = weather.map {
            Weather(it.id, it.main, it.description, it.icon.toIconUrlString())
        },
        base = base,
        main = main.let {
            Main(it.temp, it.feelsLike, it.tempMin, it.tempMax, it.pressure, it.humidity)
        },
        visibility = visibility,
        wind = wind.let {
            Wind(it.speed, it.deg)
        },
        snow = snow?.let { Snow(it.h) },
        clouds = Clouds(clouds.all),
        dt = dt,
        sys = sys.let {
            Sys(it.type, it.id, it.country, it.sunrise, it.sunset)
        },
        timezone = timezone,
        name = name,
        cod = cod,
    )
}