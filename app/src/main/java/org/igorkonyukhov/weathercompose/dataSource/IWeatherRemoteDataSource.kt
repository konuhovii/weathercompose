package org.igorkonyukhov.weathercompose.dataSource

interface IWeatherRemoteDataSource {
    suspend fun getWeather(latitude: Float, longitude: Float): WeatherResponse?
}