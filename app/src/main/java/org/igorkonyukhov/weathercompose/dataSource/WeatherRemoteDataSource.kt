package org.igorkonyukhov.weathercompose.dataSource

import org.igorkonyukhov.weatherapi.OpenWeatherResponse
import org.igorkonyukhov.weatherapi.WeatherApiProxy

class WeatherRemoteDataSource(private val weatherApiProxy: WeatherApiProxy) : IWeatherRemoteDataSource {
    override suspend fun getWeather(latitude: Float, longitude: Float): WeatherResponse? =
        weatherApiProxy.getWeather(latitude, longitude)?.toWeatherResponse()

    private fun OpenWeatherResponse.toWeatherResponse(): WeatherResponse = WeatherResponse(
        coord = WeatherResponse.Coord(coord.lon, coord.lat),
        weather = weather.map { WeatherResponse.Weather(it.id, it.main, it.description, it.icon) },
        base = base,
        main = WeatherResponse.Main(
            main.temp,
            main.feelsLike,
            main.tempMin,
            main.tempMax,
            main.pressure,
            main.humidity
        ),
        visibility = visibility,
        wind = WeatherResponse.Wind(wind.speed, wind.deg),
        snow = snow?.let { WeatherResponse.Snow(it.h) },
        clouds = WeatherResponse.Clouds(clouds.all),
        dt = dt,
        sys = WeatherResponse.Sys(sys.type, sys.id, sys.country, sys.sunrise, sys.sunset),
        timezone = timezone, id = id, name = name, cod = cod
    )
}