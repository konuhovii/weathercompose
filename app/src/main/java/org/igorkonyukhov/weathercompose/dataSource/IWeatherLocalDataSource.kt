package org.igorkonyukhov.weathercompose.dataSource

import kotlinx.coroutines.flow.Flow
import org.igorkonyukhov.weathercompose.domain.OpenWeatherEntity

interface IWeatherLocalDataSource {
    suspend fun save(response: WeatherResponse)
    fun getWeather(): Flow<OpenWeatherEntity?>
}