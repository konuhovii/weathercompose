package org.igorkonyukhov.weathercompose.domain

data class OpenWeatherEntity(
    val weather: List<WeatherEntity>,
    val main: MainEntity
)

data class WeatherEntity(
    val main: String,
    val description: String,
    val icon: String
)

data class MainEntity(
    val temp: Double,
    val feelsLike: Double,
    val tempMin: Double,
    val tempMax: Double,
    val pressure: Int,
    val humidity: Int
)