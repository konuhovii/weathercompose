package org.igorkonyukhov.weathercompose

import android.app.Application
import org.igorkonyukhov.weathercompose.di.KoinProvider

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        KoinProvider.initKoin(this@MainApplication)
    }
}