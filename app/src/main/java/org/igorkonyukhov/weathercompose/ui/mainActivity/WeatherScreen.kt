package org.igorkonyukhov.weathercompose.ui.mainActivity

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import org.igorkonyukhov.app.R as AppRes

@Composable
fun WeatherScreen(viewModel: MainViewModel) {
    when (val state = viewModel.state.collectAsStateWithLifecycle().value) {
        is MainViewModel.State.ShowWeather -> ShowWeather(state, viewModel)
        else -> {}
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ShowWeather(state: MainViewModel.State.ShowWeather, viewModel: MainViewModel) {
    val pullRefreshState = rememberPullRefreshState(
        refreshing = state.isLoading,
        onRefresh = { viewModel.handleEvent(Event.UpdateWeather) }
    )
    Column(
        modifier = Modifier
            .fillMaxSize()
            .pullRefresh(pullRefreshState)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        PullRefreshIndicator(
            refreshing = state.isLoading,
            state = pullRefreshState,
            modifier = Modifier.align(Alignment.CenterHorizontally)
        )

        state.weather?.let { weatherEntity ->
            val currentWeather = weatherEntity.weather[0]
            TitleText(stringResource(id = AppRes.string.weather_condition))
            AsyncImage(
                modifier = Modifier.size(100.dp),
                model = currentWeather.icon,
                contentDescription = stringResource(id = AppRes.string.weather_icon_description)
            )

            BodyText(stringResource(id = AppRes.string.weather_main_label, currentWeather.main))
            BodyText(
                stringResource(
                    id = AppRes.string.weather_description_label,
                    currentWeather.description
                )
            )

            SpaceBetween()
            TitleText(stringResource(id = AppRes.string.weather_main))
            BodyText(
                stringResource(
                    id = AppRes.string.weather_main_temperature,
                    weatherEntity.main.temp.toInt()
                )
            )
            BodyText(
                stringResource(
                    id = AppRes.string.weather_main_feels,
                    weatherEntity.main.feelsLike.toInt()
                )
            )
            BodyText(
                stringResource(
                    id = AppRes.string.weather_main_pressure, weatherEntity.main.pressure
                )
            )
            BodyText(
                stringResource(
                    id = AppRes.string.weather_main_humidity, weatherEntity.main.humidity
                )
            )
        }
    }
}

@Composable
private fun SpaceBetween() {
    Spacer(modifier = Modifier.height(32.dp))
}

@Composable
private fun TitleText(text: String) {
    Text(
        text = text,
        style = MaterialTheme.typography.titleLarge
    )
}

@Composable
private fun BodyText(text: String) {
    Text(
        text = text,
        style = MaterialTheme.typography.bodyMedium
    )
}