package org.igorkonyukhov.weathercompose.ui.mainActivity

import android.location.Location

sealed class Event {
    object UpdateWeather : Event()
    class GotLocation(val location: Location) : Event()
}