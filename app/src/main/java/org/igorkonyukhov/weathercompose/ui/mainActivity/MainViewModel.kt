package org.igorkonyukhov.weathercompose.ui.mainActivity

import android.location.Location
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.igorkonyukhov.weathercompose.domain.OpenWeatherEntity
import org.igorkonyukhov.weathercompose.repositories.WeatherRepository

class MainViewModel(
    private val weatherRepository: WeatherRepository
) : ViewModel() {
    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, ex ->
        Log.e(this::class.simpleName, ex.stackTraceToString())
        _sideEffect.tryEmit(SideEffect.Error(ex))
    }

    sealed class State {
        object Initialize : State()
        data class ShowWeather(
            val weather: OpenWeatherEntity? = null,
            val isLoading: Boolean = true
        ) : State()
    }

    private val _state = MutableStateFlow<State>(State.Initialize)
    val state: StateFlow<State> = _state

    sealed class SideEffect {
        class Error(val exception: Throwable) : SideEffect()
    }

    private val _sideEffect = MutableSharedFlow<SideEffect>(replay = 1)
    val sideEffect: SharedFlow<SideEffect> = _sideEffect

    private var lastLocation: Location? = null

    init {
        viewModelScope.launch {
            weatherRepository.getWeatherFromDB().collect { entity ->
                entity?.let {
                    _state.value = State.ShowWeather(
                        weather = entity,
                        isLoading = false
                    )
                }
            }
        }
    }

    fun handleEvent(event: Event) {
        when (event) {
            is Event.GotLocation -> {
                if (lastLocation == null) {
                    lastLocation = event.location
                    getWeather(event.location)
                }
            }

            Event.UpdateWeather -> lastLocation?.let { getWeather(it) }
        }
    }

    private fun getWeather(location: Location) {
        viewModelScope.launch(coroutineExceptionHandler) {
            _state.tryEmit(State.ShowWeather(isLoading = true))
            weatherRepository.getWeatherFromApi(
                location.latitude.toFloat(),
                location.longitude.toFloat()
            )
        }
    }
}