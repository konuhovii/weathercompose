package org.igorkonyukhov.weathercompose.ui.mainActivity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.igorkonyukhov.app.R
import org.igorkonyukhov.location.LocationProvider
import org.igorkonyukhov.weathercompose.ui.theme.WeatherComposeTheme
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModel()

    private val locationProvider: LocationProvider by inject()

    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted)
            observeLocationUpdates()
        else
            Toast.makeText(
                this,
                getString(R.string.explain_location_permission),
                Toast.LENGTH_LONG
            ).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WeatherComposeTheme { WeatherScreen(viewModel) }
        }
        lifecycleScope.launch {
            handleSideEffects()
        }
    }

    private suspend fun handleSideEffects() {
        viewModel.sideEffect.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .collect { sideEffect ->
                withContext(Dispatchers.Main) {
                    when (sideEffect) {
                        is MainViewModel.SideEffect.Error -> showError(sideEffect.exception)
                    }
                }
            }
    }

    private fun showError(exception: Throwable) {
        Toast.makeText(this, exception.message, Toast.LENGTH_LONG).show()
    }

    override fun onStart() {
        super.onStart()
        checkLocationPermission()
    }

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            observeLocationUpdates()
        } else {
            requestLocationPermission()
        }
    }

    private fun observeLocationUpdates() {
        locationProvider.startLocationUpdates()
        locationProvider.currentLocation.observe(this) {
            viewModel.handleEvent(Event.GotLocation(it))
        }
    }

    private fun requestLocationPermission() {
        locationPermissionRequest.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    override fun onStop() {
        super.onStop()
        locationProvider.stopLocationUpdates()
    }
}