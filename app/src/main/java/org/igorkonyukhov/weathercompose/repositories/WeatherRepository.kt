package org.igorkonyukhov.weathercompose.repositories

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import org.igorkonyukhov.weathercompose.dataSource.IWeatherLocalDataSource
import org.igorkonyukhov.weathercompose.dataSource.IWeatherRemoteDataSource
import org.igorkonyukhov.weathercompose.domain.OpenWeatherEntity

abstract class WeatherRepository {
    abstract fun getWeatherFromDB(): Flow<OpenWeatherEntity?>

    abstract suspend fun getWeatherFromApi(latitude: Float, longitude: Float)
}

class WeatherRepositoryImpl(
    private val remoteDataSource: IWeatherRemoteDataSource,
    private val localDataSource: IWeatherLocalDataSource,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : WeatherRepository() {
    override fun getWeatherFromDB(): Flow<OpenWeatherEntity?> =
        localDataSource.getWeather().flowOn(dispatcher)

    override suspend fun getWeatherFromApi(latitude: Float, longitude: Float) {
        withContext(dispatcher) {
            remoteDataSource.getWeather(latitude, longitude)?.let { localDataSource.save(it) }
        }
    }
}