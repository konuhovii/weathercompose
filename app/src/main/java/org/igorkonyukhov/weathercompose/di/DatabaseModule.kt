package org.igorkonyukhov.weathercompose.di

import org.igorkonyukhov.roomdb.WeatherDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single { WeatherDatabase.getInstance(androidContext()) }
}