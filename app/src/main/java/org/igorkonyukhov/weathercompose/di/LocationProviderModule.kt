package org.igorkonyukhov.weathercompose.di

import org.igorkonyukhov.location.LocationProvider
import org.igorkonyukhov.location.LocationProviderImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val locationProviderModule = module {
    single<LocationProvider> { LocationProviderImpl(androidContext()) }
}