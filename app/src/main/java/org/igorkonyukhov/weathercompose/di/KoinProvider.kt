package org.igorkonyukhov.weathercompose.di

import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

object KoinProvider {
    private val modules = listOf(
        locationProviderModule,
        databaseModule,
        apiModule,
        weatherDataLayerModule,
        viewModels
    )

    fun initKoin(appContext: Context) {
        startKoin {
            androidContext(appContext)
            modules(modules)
        }
    }
}