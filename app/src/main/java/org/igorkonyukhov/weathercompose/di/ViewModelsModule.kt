package org.igorkonyukhov.weathercompose.di

import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module
import org.igorkonyukhov.weathercompose.ui.mainActivity.MainViewModel

val viewModels = module {
    viewModelOf(::MainViewModel)
}