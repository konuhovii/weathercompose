package org.igorkonyukhov.weathercompose.di

import org.igorkonyukhov.weatherapi.WeatherApiProxy
import org.igorkonyukhov.weatherapi.buildOkHttpClient
import org.igorkonyukhov.weatherapi.buildRetrofit
import org.igorkonyukhov.weatherapi.createOpenWeatherApi
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.module

val apiModule = module {
    single { buildOkHttpClient() }
    single { buildRetrofit(get()) }
    single { createOpenWeatherApi(get()) }
    factoryOf(::WeatherApiProxy)
}