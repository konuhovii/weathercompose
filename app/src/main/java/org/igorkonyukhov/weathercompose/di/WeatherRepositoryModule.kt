package org.igorkonyukhov.weathercompose.di

import org.igorkonyukhov.weathercompose.dataSource.IWeatherLocalDataSource
import org.igorkonyukhov.weathercompose.dataSource.IWeatherRemoteDataSource
import org.igorkonyukhov.weathercompose.dataSource.WeatherRemoteDataSource
import org.igorkonyukhov.weathercompose.dataSource.WeatherRoomLocalDataSource
import org.igorkonyukhov.weathercompose.repositories.WeatherRepository
import org.igorkonyukhov.weathercompose.repositories.WeatherRepositoryImpl
import org.koin.dsl.module

val weatherDataLayerModule = module {
    factory<IWeatherLocalDataSource> { WeatherRoomLocalDataSource(get()) }
    factory<IWeatherRemoteDataSource> { WeatherRemoteDataSource(get()) }
    factory<WeatherRepository> {
        WeatherRepositoryImpl(get(), get())
    }
}