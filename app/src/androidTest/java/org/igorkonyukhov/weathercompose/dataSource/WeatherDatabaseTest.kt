package org.igorkonyukhov.weathercompose.dataSource

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import java.io.IOException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull
import org.igorkonyukhov.roomdb.Clouds
import org.igorkonyukhov.roomdb.Coord
import org.igorkonyukhov.roomdb.Main
import org.igorkonyukhov.roomdb.Snow
import org.igorkonyukhov.roomdb.Sys
import org.igorkonyukhov.roomdb.WeatherDBO
import org.igorkonyukhov.roomdb.WeatherDao
import org.igorkonyukhov.roomdb.WeatherDatabase
import org.igorkonyukhov.roomdb.Wind
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WeatherDatabaseTest {
    private lateinit var dao: WeatherDao
    private lateinit var db: WeatherDatabase
    private val coordMock = Coord(1.11, 1.11)
    private val mockDbo = WeatherDBO(
        coord = coordMock,
        weather = listOf(),
        base = "base",
        main = Main(0.0, 0.0, 0.0, 0.0, 0, 0),
        visibility = 0,
        wind = Wind(0.0, 0),
        snow = Snow(0.0),
        clouds = Clouds(0),
        dt = 0,
        sys = Sys(0, 0, "", 0, 0),
        timezone = 0,
        name = "name",
        cod = 0
    )

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, WeatherDatabase::class.java
        ).build()
        dao = db.weatherDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun insertToDbSuccessful() {
        runBlocking {
            launch(Dispatchers.IO) {
                dao.insert(mockDbo)
                withTimeoutOrNull(100) {
                    dao.getWeather().collect {
                        println(it)
                        assert(it?.coord == coordMock)
                    }
                }
            }.join()
        }
    }
}