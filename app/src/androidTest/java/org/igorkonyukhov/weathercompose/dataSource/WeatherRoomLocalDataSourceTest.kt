package org.igorkonyukhov.weathercompose.dataSource

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import java.io.IOException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withTimeoutOrNull
import org.igorkonyukhov.roomdb.WeatherDatabase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WeatherRoomLocalDataSourceTest {
    private lateinit var db: WeatherDatabase
    private val mockResponse = WeatherResponse(
        id = 0,
        coord = WeatherResponse.Coord(0.0, 0.0),
        weather = listOf(),
        base = "base",
        main = WeatherResponse.Main(11.0, 0.0, 0.0, 0.0, 0, 0),
        visibility = 0,
        wind = WeatherResponse.Wind(0.0, 0),
        snow = WeatherResponse.Snow(0.0),
        clouds = WeatherResponse.Clouds(0),
        dt = 0,
        sys = WeatherResponse.Sys(0, 0, "", 0, 0),
        timezone = 0,
        name = "name",
        cod = 0
    )
    private lateinit var localDataSource: IWeatherLocalDataSource

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, WeatherDatabase::class.java
        ).build()
        localDataSource = WeatherRoomLocalDataSource(db)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun insertToDbSuccessful() {
        runTest {
            launch(Dispatchers.IO) {
                localDataSource.save(mockResponse)
                withTimeoutOrNull(100) {
                    localDataSource.getWeather().collect {
                        println(it)
                        assert(it?.main?.temp == 11.0)
                    }
                }
            }.join()
        }
    }
}