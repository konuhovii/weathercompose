package org.igorkonyukhov.weathercompose.repositories

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import java.io.IOException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.igorkonyukhov.roomdb.WeatherDatabase
import org.igorkonyukhov.weatherapi.WeatherApiProxy
import org.igorkonyukhov.weatherapi.buildOkHttpClient
import org.igorkonyukhov.weatherapi.buildRetrofit
import org.igorkonyukhov.weatherapi.createOpenWeatherApi
import org.igorkonyukhov.weathercompose.dataSource.IWeatherLocalDataSource
import org.igorkonyukhov.weathercompose.dataSource.IWeatherRemoteDataSource
import org.igorkonyukhov.weathercompose.dataSource.WeatherRemoteDataSource
import org.igorkonyukhov.weathercompose.dataSource.WeatherRoomLocalDataSource
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WeatherRepositoryImplTest {
    private lateinit var db: WeatherDatabase
    private lateinit var localDataSource: IWeatherLocalDataSource
    private lateinit var remoteDataSource: IWeatherRemoteDataSource
    private val apiProxy = WeatherApiProxy(
        createOpenWeatherApi(
            buildRetrofit(buildOkHttpClient())
        )
    )
    private val lat = 55.75f
    private val lon = 49.18f
    private lateinit var repositoryImpl: WeatherRepository

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, WeatherDatabase::class.java
        ).build()
        localDataSource = WeatherRoomLocalDataSource(db)
        remoteDataSource = WeatherRemoteDataSource(apiProxy)
        repositoryImpl = WeatherRepositoryImpl(remoteDataSource, localDataSource)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun repositoryRequestIsUpdatingDatabase() {
        runTest {
            launch {
                repositoryImpl.getWeatherFromApi(lat, lon)
                repositoryImpl.getWeatherFromDB().collectLatest {
                    println(it)
                    assert(it != null)
                    delay(2000)
                    this.cancel()
                }
            }.join()
        }
    }
}