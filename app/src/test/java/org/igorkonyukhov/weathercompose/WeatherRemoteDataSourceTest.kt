package org.igorkonyukhov.weathercompose

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.igorkonyukhov.weatherapi.WeatherApiProxy
import org.igorkonyukhov.weatherapi.buildOkHttpClient
import org.igorkonyukhov.weatherapi.buildRetrofit
import org.igorkonyukhov.weatherapi.createOpenWeatherApi
import org.igorkonyukhov.weathercompose.dataSource.IWeatherRemoteDataSource
import org.igorkonyukhov.weathercompose.dataSource.WeatherRemoteDataSource
import org.igorkonyukhov.weathercompose.dataSource.WeatherResponse
import org.junit.Test

class WeatherRemoteDataSourceTest {
    private val apiInteractor = WeatherApiProxy(
        createOpenWeatherApi(
            buildRetrofit(buildOkHttpClient())
        )
    )
    private val remoteDataSource: IWeatherRemoteDataSource = WeatherRemoteDataSource(apiInteractor)
    private val lat = 55.75f
    private val lon = 49.18f

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testWeatherRequest() = runTest {
        launch(Dispatchers.IO) {
            val response = remoteDataSource.getWeather(
                latitude = lat, longitude = lon
            )
            assert(response != null)
            assert(response is WeatherResponse)
            println(response)
        }.join()
    }
}