package org.igorkonyukhov.location

import android.annotation.SuppressLint
import android.content.Context
import android.icu.text.DecimalFormat
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import java.util.concurrent.TimeUnit

class LocationProviderImpl(context: Context) : LocationProvider() {

    private val fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    private val locationRequest: LocationRequest = LocationRequest.Builder(
        Priority.PRIORITY_BALANCED_POWER_ACCURACY,
        TimeUnit.MINUTES.toMillis(60)
    ).build()

    private val df = DecimalFormat("#.####")
    private var locationUpdatesStarted = false

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            locationResult.lastLocation?.let {
                if (currentLocation.value == null
                    || (df.format(currentLocation.value?.latitude) != df.format(it.latitude)
                            && df.format(currentLocation.value?.longitude) != df.format(it.longitude))
                )
                    (currentLocation as MutableLiveData).postValue(it)
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun startLocationUpdates() {
        if (!locationUpdatesStarted) {
            locationUpdatesStarted = true
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        }
    }

    override fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
}