package org.igorkonyukhov.location

import android.annotation.SuppressLint
import android.location.Location
import androidx.lifecycle.LiveData

abstract class LocationProvider {
    val currentLocation: LiveData<Location> = SingleLiveEvent()

    @SuppressLint("MissingPermission")
    abstract fun startLocationUpdates()

    abstract fun stopLocationUpdates()
}